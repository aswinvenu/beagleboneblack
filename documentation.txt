/* Set up the linux build environment */

# export PATH=/opt/gcc-arm-10.3-2021.07-x86_64-arm-none-linux-gnueabihf/bin:$PATH

# set ARCH and CROSS_COMPILE

# alias armmake='make ARCH=arm CROSS_COMPILE=arm-none-linux-gnueabihf-'

/* Download and build u-boot */

# git clone git://git.denx.de/u-boot.git

# cd u-boot
# armmake distclean
# armmake am335x_evm_defconf
# armmake make DEVICE_TREE=am335x-boneblack

This will generate MLO and u-boot.bin

Crate a new bootable partition in the SD card and copy these files into that

(use fdisk to create a new partition )

# sudo mkfs.vfat -a -F 16 -n boot /dev/sdb1
# sudo mkfs.ext4 -L rootfs /dev/sdb2

# mount /dev/sdb1 to /mnt/boot
# cp MLO /mnt/boot
# cp u-boot.img /mnt/boot
 
/* Download the latest linux kernel */

Install dependencies for building linux kernel

# sudo apt install libgmp-dev
# sudo apt install libmpc-dev

# git clone https://github.com/beagleboard/linux.git
# cd linux
# armmake distclean
# armmake multi_v7_defconfig / omap2plus_defconfig

/* Make uImage */

# armmake 
/* For only dtbs */
# armmake dtbs
/* For installing modules */ 
# armmake modules_install INSTALL_MOD_PATH=<path to rootfs>

# sudo apt install u-boot-tools
or
# mkimage -A arm -O linux -T kernel -C none -a 0x82000000 -e 0x82000000 -n "Linux kernel uImage" -d arch/arm/boot/zImage-5.10.100-g7a7a3af903 uImage

/* copy the linux image to the boot partition */
# cp uImage /mnt/boot/

/* now create a bootscript for uboot */
# mkdir /mnt/boot/extlinux
# vim /mnt/boot/extlinux/extlinux.conf

now copy the below boot script

label Linux microSD
    kernel /zImage
    devicetree /boot/am335x-boneblack.dtb
    append console=ttyS0,115200n8 root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait earlyprintk mem=512M

with the new distro-boot support in u-boot, it looks for extlinux.conf.
In its absence a U-Boot specific script called boot.scr or boot.scr.uimg.
scanned in the following order

SD Card
Internal flash
External USB storage
External server whose local ip frpm DHCP request at the address provided by the ${serverip} environment variable

/* Create rootfs */

Download busybox

# wget https://busybox.net/downloads/busybox-1.36.1.tar.bz2
# tar xf busybox-1.36.1.tar.z2
# cd busybox-1.36.1
# armmake defconfig
# armmake menuconfig

add any extra parameters if required 
Make it static ( for now )

# armmake install CONFIG_PREFIX=/media/aswin/rootfs

Create directories with the name
etc proc sys dev erc/init usr/lib

mkdir etc proc sys dev erc/init usr/lib
mkdir home home/root

Create init file

# vim etc/init.d/rcS

#!bin/sh
mount -t proc none /proc
mount -t sysfs none /sys
mount -t tmpfs none /var
mount -t tmpfs none /dev
echo /sbin/mdev > /proc/sys/kernel/hotplug
/sbin/mdev -s
mkdir var
mkdir var/log

chmod +x etc/init.d/rcS
